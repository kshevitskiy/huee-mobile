// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'swiper/dist/css/swiper.css'
import './styles/style.scss'

import Vue from 'vue'
import App from './App'
import VueI18n from 'vue-i18n'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import VeeValidate from 'vee-validate'

Vue.use(VueI18n)
Vue.use(VueAwesomeSwiper)
Vue.use(VeeValidate)

Vue.config.productionTip = false

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: 'pl',
  fallbackLocale: 'pl',
  messages: {
    'en': require('./locale/en.json'),
    'pl': require('./locale/pl.json')
  }
})

Vue.prototype.$locale = {
  change (language) {
    i18n.locale = language
  },
  current () {
    return i18n.locale
  }
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  i18n,
  components: {
    App
  },
  template: '<App/>'
})
