<?php // for MailChimp API v3.0

include('MailChimp.php');  // path to API wrapper downloaded from GitHub

use \DrewM\MailChimp\MailChimp;

function storeAddress() {

    $key        = "a90b2b2f59c55d731f5de08a816efc05-us17";
    $list_id    = "a9e6af5634";

    // mailchimp fields
    $merge_vars = array(
        'NAME' => $_POST['NAME']
    );

    $terms = $_POST['TERMS'];
    $terms_accepted = false;

    if ($terms === 'true') {
        $terms_accepted = true;
    }

    // mailchimp interests
    $checkboxes = array(
        '1b7d822f3a' => $terms_accepted
    );

    $mc = new MailChimp($key);

    // add the email to your list
    $result = $mc->post('/lists/'.$list_id.'/members', array(
            'email_address'     => $_POST['EMAIL'],
            'status'            => 'subscribed',
            'merge_fields'      => $merge_vars,
            'interests'         => $checkboxes,
            'update_existing'   => true
        )
    );

    return json_encode($result);

}

// If being called via ajax, run the function, else fail
if ($_POST['NAME'] && $_POST['EMAIL'] && $_POST['TERMS']) {
    echo storeAddress(); // send the response back through Ajax
} else {
    echo '{"detail": "One or more required fields are not completed"}';
}