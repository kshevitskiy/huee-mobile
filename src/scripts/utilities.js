var UTILITIES = {

  trimText: function (str, length, ending) {
    if (length == null) {
      length = 100
    }

    if (ending == null) {
      ending = '...'
    }

    if (str.length > length) {
      let trimmedString = str.substr(0, length)
      trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(' ')))
      return trimmedString + ending
    } else {
      return str
    }
  },

  orientation: {

    isLandscape: function () {
      return screen.orientation.angle === 90 || screen.orientation.angle === -90
      // return window.matchMedia('(orientation: landscape)').matches
    }
  },

  isMobile: {

    Android: function () {
      return navigator.userAgent.match(/Android/i)
    },
    BlackBerry: function () {
      return navigator.userAgent.match(/BlackBerry/i)
    },
    iOS: function () {
      return navigator.userAgent.match(/iPhone|iPad|iPod/i)
    },
    Opera: function () {
      return navigator.userAgent.match(/Opera Mini/i)
    },
    Windows: function () {
      return navigator.userAgent.match(/IEMobile/i)
    },
    any: function () {
      return (UTILITIES.isMobile.Android() || UTILITIES.isMobile.BlackBerry() || UTILITIES.isMobile.iOS() || UTILITIES.isMobile.Opera() || UTILITIES.isMobile.Windows())
    }
  },

  isDevice: {

    iPhone5Below: function () {
      return UTILITIES.isMobile.iOS() && screen.availHeight <= 568
    }
  },

  isBrowser: {

    Safari: function () {
      return !!navigator.userAgent.match(/Version\/[\d.]+.*Safari/)
    }
  },

  isNavigator: {

    Standalone: function () {
      return window.navigator.standalone
    }
  },

  serialize: function (form) {
    var field = []
    var l = []
    var s = []
    if (typeof form === 'object' && form.nodeName === 'FORM') {
      var len = form.elements.length
      for (var i = 0; i < len; i++) {
        field = form.elements[i]
        if (field.name && !field.disabled && field.type !== 'file' && field.type !== 'reset' && field.type !== 'submit' && field.type !== 'button') {
          if (field.type === 'select-multiple') {
            l = form.elements[i].options.length
            for (var j = 0; j < l; j++) {
              if (field.options[j].selected) {
                s[s.length] = encodeURIComponent(field.name) + '=' + encodeURIComponent(field.options[j].value)
              }
            }
          } else if ((field.type !== 'checkbox' && field.type !== 'radio') || field.checked) {
            s[s.length] = encodeURIComponent(field.name) + '=' + encodeURIComponent(field.value)
          }
        }
      }
    }
    return s.join('&').replace(/%20/g, '+')
  }

}

export default {
  trimText: UTILITIES.trimText,
  isMobile: UTILITIES.isMobile,
  isBrowser: UTILITIES.isBrowser,
  isNavigator: UTILITIES.isNavigator,
  orientation: UTILITIES.orientation,
  isDevice: UTILITIES.isDevice,
  serialize: UTILITIES.serialize
}
